import { Directive, ElementRef } from '@angular/core';
import { NgControl } from '@angular/forms';
import * as moment from 'moment';

declare const Pikaday;

@Directive({
  selector: '[appDatapicker]',
})
export class DatapickerDirective {

  constructor(
    private el: ElementRef,
    private control: NgControl,
  ) {
    const that = this;
    const picker = new Pikaday({
      field: el.nativeElement,
      format: 'DD/MM/YYYY',
      toString(date, format) {
        return moment(date).format(format);
      },
      onSelect: data => control.control.setValue(moment(data).format('YYYY-DD-MM'))
    });
  }
}
