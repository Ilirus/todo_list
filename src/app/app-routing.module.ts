import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePageComponent } from './components/home-page/home-page.component';
import { Error404Component } from './components/error-404/error-404.component';
import { ItemsListComponent } from './components/items-list/items-list.component';

export const routes: Routes = [
        {
            path: '',
            component: HomePageComponent
        }, {
            path: 'items-list',
            component: ItemsListComponent
        }, {
            path: '404',
            component: Error404Component
        }, {
            path: '**',
            redirectTo: '/404'
        }
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
