import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringToArray'
})
export class StringToArrayPipe implements PipeTransform {

  transform(value: string | number, args?: string[]): Array<number> {
    const res = [];
    let _value: number;
    if (typeof value === 'string') {
      _value = value.length;
    }
    if (typeof value === 'number') {
      _value = value;
    }
    for (let i = 0; i < _value; i++) {
      res.push(i + 1);
    }
    return res;
  }

}
