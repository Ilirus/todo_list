import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showPage'
})
export class ShowPage implements PipeTransform {

  transform(value: Array<any>, args: {page: number, elemsOnPage: number}): Array<any> {
    const res = [];
    for (let i = 0; i <= (args.elemsOnPage - 1); i++) {
      if (value[((args.page - 1) * args.elemsOnPage) + i]) {
        res.push(value[((args.page - 1) * args.elemsOnPage) + i]);
      }
    }
    return res;
  }

}
