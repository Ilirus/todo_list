import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutLongString'
})
export class CutLongStringPipe implements PipeTransform {

  transform(value: string, maxLength: number): string {
    if (value.length > maxLength) {
      return value = value.slice(0, maxLength) + '...';
    } else {
      return value;
    }
  }

}
