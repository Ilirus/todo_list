import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showAmount'
})
export class ShowAmountPipe implements PipeTransform {

  transform(value: Array<any>, args: {elem: number, amountShow: number}): Array<any> {
    const res = [];
    let after = Math.ceil(args.amountShow / 3) + +(args.amountShow / 4).toFixed() - 1;
    let before = +(args.amountShow / 3).toFixed(0) + 1;
    if (args.elem - before >= 0 && args.elem + after < value.length) {
      addItemsBefore();
      addItemsAfter();
    } else {
      addItemsAfter(true);
      addItemsAfter();
    }
    function addItemsBefore(next?) {
      for (let i = before; i > 0; i--) {
        if (value[args.elem - i] !== undefined) {
          if (!next) {
            res.push(value[args.elem - i]);
          }
        } else {
          after++;
        }
      }
      if (next) {
        addItemsAfter();
      }
    }
    function addItemsAfter(next?) {
      for (let i = 0; i < after; i++) {
        if (value[args.elem + i] !== undefined) {
          if (!next) {
            res.push(value[args.elem + i]);
          }
        } else {
          before++;
        }
      }
      if (next) {
        addItemsBefore();
      }
    }
    return res;
  }

}
