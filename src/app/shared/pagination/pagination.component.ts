import {
  Component,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  HostBinding
} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.pug',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {

  private _elementsAmount: number;

  @Input() startPage = 1;
  @Input() elementsOnPage = 1;
  @Input() set elementsAmount (elementsAmount: number) {
    this._elementsAmount = elementsAmount;
    this.countPages();
  }
  @Output() changeCurrentPage = new EventEmitter<number>();

  private currentPage: number;
  private pages: number;

  constructor() {
    this.countPages();
    this.currentPage = this.startPage;
  }

  private countPages() {
    this.pages = Math.ceil(this._elementsAmount / this.elementsOnPage);
  }

  private changePage(page: number) {
    if (page <= 0) {
      page = 1;
    }
    if (page > this.pages) {
      page = this.pages;
    }
    this.currentPage = page;
    this.changeCurrentPage.emit(this.currentPage);
  }

  private checkCurrentPage(type) {
    switch (type) {
      case 'more':
        return this.currentPage - +(this.elementsOnPage / 3).toFixed(0) > 1 && this.elementsOnPage < this.pages;
      case 'less':
        return (
          this.currentPage + Math.ceil(this.elementsOnPage / 3) +
          (
            +(this.elementsOnPage / 4).toFixed(0) - 1
          ) < this.pages &&
          this.elementsOnPage < this.pages);
      default:
        break;
    }
  }
}
