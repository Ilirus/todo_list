import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './pagination/pagination.component';
import { StringToArrayPipe } from './pipes/string-to-array.pipe';
import { ShowPage } from './pipes/show-page.pipe';
import { ShowAmountPipe } from './pipes/show-amount.pipe';
import { CutLongStringPipe } from './pipes/cut-long-string.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    PaginationComponent,
    StringToArrayPipe,
    ShowPage,
    ShowAmountPipe,
    CutLongStringPipe
  ],
  declarations: [
    PaginationComponent,
    StringToArrayPipe,
    ShowPage,
    ShowAmountPipe,
    CutLongStringPipe
  ]
})
export class SharedModule { }
