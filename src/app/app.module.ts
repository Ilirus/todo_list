import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomePageComponent } from './components/home-page/home-page.component';
import { Error404Component } from './components/error-404/error-404.component';
import { ItemsListComponent } from './components/items-list/items-list.component';
import { DatapickerDirective } from './directives/datapicker/datapicker.directive';
import { SharedModule } from './shared/shared.module';

import { MatTooltipModule } from '@angular/material/tooltip';

import { ApiService } from './services/api/api.service';
import { ItemsService } from './services/items/items.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    Error404Component,
    ItemsListComponent,
    DatapickerDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    MatTooltipModule
  ],
  providers: [
    ApiService,
    ItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
