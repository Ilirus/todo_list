export interface ItemInterface {
    id: number;
    title: string;
    description: string;
    created_at: string;
}

export function isItemInterface(object: any): object is ItemInterface {
    return (
        typeof (object as ItemInterface).id === 'number' &&
        typeof (object as ItemInterface).title === 'string' &&
        typeof (object as ItemInterface).description === 'string' &&
        typeof (object as ItemInterface).created_at === 'string'
    );
}