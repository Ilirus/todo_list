import { Injectable } from '@angular/core';
import { ItemInterface } from '../../interface/item-interface';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ApiService {

  private apiUrls = {
    itemsList: './mockup/todo-list.json'
  };

  constructor(private http: HttpClient) { }

  getItemsList (): Observable<Object> {
    return this.http.get(this.apiUrls['itemsList']);
  }
}
