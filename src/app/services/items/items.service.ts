import { Injectable } from '@angular/core';
import { ItemInterface, isItemInterface } from '../../interface/item-interface';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import * as moment from 'moment';

@Injectable()
export class ItemsService {

  private itemsListSubject = new Subject<Array<ItemInterface>>();
  private itemsList = new Array<ItemInterface>();
  private lastId: number;

  constructor() {
    this.getItemsList().subscribe((list) => {
      this.itemsList = list;
    });
  }

  setItemsList(list: Array<ItemInterface>) {
    this.lastId = this.findLastId(list);
    this.itemsListSubject.next(list);
  }

  getItemsList(): Observable<Array<ItemInterface>> {
      return this.itemsListSubject.asObservable();
  }

  addItem(item: object) {
    const _item = {
      id: this.lastId + 1,
      created_at: moment().format('YYYY-DD-MM'),
      title: item['title'],
      description: item['description']
    };
    if (isItemInterface(_item)) {
      const newItemsList = [_item, ...this.itemsList];
      this.setItemsList(newItemsList);
    } else {
      alert('Something went wrong');
      console.log(item);
    }
  }

  deleteItem(id: number) {
    const newItemsList = this.itemsList.filter(elem => {
      return elem.id !== id;
    });
    this.setItemsList(newItemsList);
  }

  private findLastId (array: Array<any>) {
    let lastId = 0;
    array.forEach((elem) => {
      if (elem.id > lastId) {
        lastId = elem.id;
      }
    });
    return lastId;
  }

  filterItems(filters: Array<any>): Array<ItemInterface> {
    return this.itemsList.filter(elem => {
      let suitable = true;
      for (const filter in filters) {
        if (filters.hasOwnProperty(filter)) {
          switch (filter) {
            case 'title':
              if (filters[filter].length > 0) {
                suitable = elem[filter].toLowerCase().indexOf(filters[filter].toLowerCase()) !== -1;
              }
              break;
            case 'created_at':
              if (filters[filter].length > 0) {
                suitable = (
                  moment(elem[filter], 'YYYY-DD-MM').format('x') ===
                  moment(filters[filter], 'YYYY-DD-MM').format('x')
                );
              }
              break;
            default:
              break;
          }
          if (!suitable) {
            return;
          }
        }
      }
      return suitable;
    });
  }
}
