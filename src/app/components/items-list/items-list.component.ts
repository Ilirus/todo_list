import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApiService } from '../../services/api/api.service';
import { ItemsService } from '../../services/items/items.service';
import { ItemInterface } from '../../interface/item-interface';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.pug',
  styleUrls: ['./items-list.component.scss']
})

export class ItemsListComponent implements OnInit {

  private model;
  private itemsList = new Array<ItemInterface>();
  private formCreateItem: FormGroup;
  private formFilters: FormGroup;
  private listConfig = {
    currentPage: 1,
    elemsOnPage: 5
  };

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private items: ItemsService
  ) {
    this.api.getItemsList().subscribe((resp: any ) => {
      this.items.setItemsList(resp);
    });

    this.items.getItemsList().subscribe(resp => {
      console.log(resp)
      this.itemsList = resp;
    });

    this.formCreateItem = fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.formFilters = fb.group({
      title: '',
      created_at: ''
    });
    this.formFilters.valueChanges.subscribe(data => {
      this.useFilters(data);
    });
  }

  ngOnInit() {
  }

  private deleteItem (id: number) {
    this.items.deleteItem(id);
  }

  private addItem (item) {
    this.items.addItem(item);
  }

  private changeCurrentPage (event) {
    this.listConfig.currentPage = event;
  }

  private useFilters(filters) {
    console.log(filters);
    this.itemsList = this.items.filterItems(filters);
    console.log(this.itemsList)
  }

}
